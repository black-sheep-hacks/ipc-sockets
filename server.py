#!/usr/bin/env python3

import socket
import sys
import os

ADDRESS = "./local.sock"

if os.path.exists(ADDRESS):
    os.unlink(ADDRESS)


print("# Creating server...")
server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
try:
    server.bind(ADDRESS)
    server.listen(1)
    print("# Server created")
    while True:
        connection, client = server.accept()
        if data := connection.recv(0x1_000):
            print(f"Received: {data.decode('utf-8')}")
        else:
            print("Error")
            break
finally:
    server.close()