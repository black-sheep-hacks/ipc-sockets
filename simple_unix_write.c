#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/un.h>


int main(int argc, char *argv[])
{
    int sockfd = 0, len = 0, socket_path_len = 0;
    const char* message = "Hello, world!\n";
    const char* socket_path = "local.sock";
    struct sockaddr_un serv_addr; 

    if((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        perror("\nCould not create socket \n");
        return 1;
    } 

    serv_addr.sun_family = AF_UNIX;
    socket_path_len = strlen(socket_path);
    strncpy(serv_addr.sun_path, socket_path, socket_path_len+1); 
    len = socket_path_len + 1 + sizeof(serv_addr.sun_family);
    printf("%s: %d\n", socket_path, socket_path_len);

    if (connect(sockfd, (struct sockaddr *)&serv_addr, len) == -1)
    {
       perror("\nConnection failed \n");
       return 1;
    } 

    if (write(sockfd, message, strlen(message)) == -1)
    {
        perror("\nWriting data unsuccessful\n");
        return 1;
    }

    close(sockfd);

    return 0;
}